
Template.header.events({
    'click #logout'(){
        AccountsTemplates.logout();
        FlowRouter.go('/');
    }
});
var peer;
person = {'id':'',
                'connected':false};
connected = false;
conn='';
Template.sidemenu.onCreated(function(){
    //make this global value
    peer = new Peer({key: 'plw7cjoe8a4te29'});
    peer.on('open',function(id){
        $("#peerID").text(id);
    });
});
Template.sidemenu.onRendered(function () {
    this.subscribe('groups');
    this.subscribe('users');
    this.subscribe('files');
});

connected = false;

Template.sidemenu.helpers({
    groups(){
        return Groups.find();
    },
    users(){
        return Meteor.users.find({_id: {$ne: Meteor.userId()}});
    },
    isUserInGroup(){

        if(Groups.find({$and:[{_id:this._id},{$or:[{members:Meteor.userId()}, {owner:Meteor.userId()}]}]}).count()==1){
           return true;
        }
        return false;
    },
    getPeer(){//someone will connect to us
        peer.on('connection', connect);            

            return true;
    }
});
Template.sidemenu.events({
    'click #addNewGroup'(){
        $('#newGroupModel').modal('show');
    },
    'click #addGroup'(){
        let groupName = $('#groupName').val();
        if (groupName) {
            Meteor.call('addGroup', {name: groupName}, function (err) {
                if (err) {
                    toastr.error(err.message);
                } else {
                    toastr.success("Successfully added");
                    $('#newGroupModel').modal('hide');
                }
            });
        } else {
            toastr.warning("Please, insert group name.");
        }
    },
    'click #joinGroup'(){
        let id=this._id;
            if(id){
                Meteor.call('addUserToGroup',id,function (err) {
                    if(err){
                        toastr.error(err.message);
                    }else{
                       toastr.success('joined');
                    }
                })
            }


    },
    'click #exitGroup'(){
        let groupId=this._id;
        if(groupId){
            Meteor.call('removeUserFromGroup',groupId,function (err) {
                if(err){
                    toastr.error(err.message);
                }
            });
        }
    },
    'click #connectPeer'(){

    if(person.connected){
        conn.close();
        disconnect();
    }else{
      let input = $("#otherPeerInput").val();
      if(!(input == "")){
        $(".progress-bar").width("50%");

        //establish connection with given ID
        let connection = peer.connect(input);
        connection.on('open', function(){            
            connect(connection);
        });
        }
    }
    }
});

function connect(connection){
    conn = connection;
    //after establishing connection
    $("#otherPeerID").text(conn.peer);
    $("#otherPeerInput").prop('disabled',true);     
    $(".progress-bar").width("100%");
    $("#connectPeer").prop('class', 'btn btn-danger');
    $("#connectPeer").html('Disconnect');

    person.connected = true;

    //once the data has been received
    conn.on('data', function(data){
        var blob = new Blob([data.file], {type: data.filetype});
        var url = URL.createObjectURL(blob);

        addFile({
            'name': data.filename,
            'url': url
        });
    });
};

function disconnect(){
    //after disconnection
    $("#otherPeerID").text("");
    $("#otherPeerInput").prop('disabled',false);     
    $(".progress-bar").width("0%");
    $("#connectPeer").prop('class', 'btn btn-info');
    $("#connectPeer").html('Connect');
    $(".table tbody").remove();
    person.connected = false;
}

function addFile(file) {
        
        var file_name = file.name;
        var file_url = file.url;

        $("#sharedFiles")
        .append("<tr><td><a href="+file_url+" download="+file_name+" >"+file_name+"</a></td></tr>");

    }
