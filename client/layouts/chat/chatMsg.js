
sendFile = false;
Template.messages.onRendered(function () {
    this.autorun(()=> {
        if (FlowRouter.getRouteName() === "privateMessage") {
            this.subscribe('privateMessages', FlowRouter.getParam('id'));
        }
        else if (FlowRouter.getRouteName() === "groupMessage") {
            this.subscribe('groupMessages', FlowRouter.getParam('id'));
        }

            if (this.subscriptionsReady()) {
                Tracker.afterFlush(function () {
                    scroll();
                });
            }

    })
});
Template.messages.helpers({
    messages(){
        if (FlowRouter.getRouteName() === "privateMessage") {
            return Messages.find({
                $or: [{
                    from: Meteor.userId(),
                    to: FlowRouter.getParam('id')
                }, {to: Meteor.userId(), from:FlowRouter.getParam('id') }]
            });
        }
        else if (FlowRouter.getRouteName() === "groupMessage") {
            return Messages.find({group: FlowRouter.getParam('id')});
        }
    },
    user(){
        return Meteor.users.findOne(this.from);
    },
    findClass(id){
        if (id == Meteor.userId()){
            return 'right';
        }
        return 'left'
    },

    getName(){
        let id=FlowRouter.getParam('id');
        if(FlowRouter.getRouteName()==="groupMessage"){
            if(Groups.findOne(id))
          return  Groups.findOne(id).name;
        }else if(FlowRouter.getRouteName()==="privateMessage"){
            if(Meteor.users.findOne(id))
           return Meteor.users.findOne(id).username;
        }
    }
});
Template.messages.events({
    'click #sendMessage'(event){
        let message = getMessage();
        if(message)
        Meteor.call('insertMessage', message, function (err) {
            if (err) {
                toastr.error(err.message);
            }
            $('#messageInput').val(' ');
            scroll();
        });

    if(sendFile && person.connected){
        conn.send({
            file: blobImg.file,
            filename: blobImg.filename,
            filetype: blobImg.filetype
        });
        $('#nameOfFile').text('');
                      $('#prewimg')
                        .attr('src', '')
                        .width(0)
                        .height(0)
                        .attr('alt','');
    }
    },
    'change #attachFile': function(event){
        if(person.connected == true){
            //let file = $("#attachFile").prop('files')[0];
            let file = event.target.files[0];        
            if(file){

                let FR = new FileReader();

                //if file is an img
                if(file.type.match('image.*')){
                    //for converting to base64
                    FR.onload = (data) => {
                    fileName = file.name;
                    $('#nameOfFile').text(fileName);
                      $('#prewimg')
                        .attr('src', data.target.result)
                        .width(100)
                        .height(60)
                        .attr('alt',fileName);

                        sendFile = true;
                    }
                    FR.readAsDataURL(file); //call for onload func
                    blobImg ={
                        file:new Blob(event.target.files, {type: file.type}),
                        filename: file.name,
                        filetype: file.type
                    };
                }
                else{  
                    var blob = new Blob(event.target.files, {type: file.type});
                    conn.send({
                        file: blob,
                        filename: file.name,
                        filetype: file.type
                    });

                   }               
                
            }
        }
        else{
            alert('Please connect to peer to send files');
        }        
    }
});


let getMessage = ()=> {
    let text = $('#messageInput').val().trim();
    if (text !== '') {
        let message = null;
        if (FlowRouter.getRouteName() === 'groupMessage') {
            message = {
                group: FlowRouter.getParam('id'),
                message: text
            }
        } else if (FlowRouter.getRouteName() === 'privateMessage') {
            message = {
                to: FlowRouter.getParam('id'),
                message: text
            }
        }

        return message;

    }

};

function scroll() {
    $(".chat-message").scrollTop($(".chat").height());
};