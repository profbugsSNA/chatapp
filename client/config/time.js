/**
 * Created by saria on 12/9/16.
 */
Template.registerHelper('messageTimestamp', (timestamp) => {
    if (timestamp) {
        let today = moment().format('YYYY-MM-DD'),
            datestamp = moment(timestamp).format('YYYY-MM-DD'),
            isBeforeToday = moment(today).isAfter(datestamp),
            format = isBeforeToday ? 'MMMM Do, YYYY hh:mm a' : 'hh:mm a';
        return moment(timestamp).format(format);
    }
});