/**
 * Created by saria on 12/8/16.
 */
Meteor.publish('groups', function () {
    if (!this.userId) {
        throw new Meteor.Error("Not authorized");
    }
    return Groups.find();
});
Meteor.publish('users', function () {
    if (!this.userId) {
        throw new Meteor.Error("Not authorized");
    }
    return Meteor.users.find();
});
Meteor.publish('groupMessages', function (id) {
    if (!this.userId) {
        throw new Meteor.Error("Not authorized");
    }
    check(id, String);
    console.log(Groups.find({$and:[{_id:id},{$or:[{members:this.userId},{owner:this.userId}]}]}).fetch())
    if(Groups.find({$and:[{_id:id},{$or:[{members:this.userId},{owner:this.userId}]}]}).count()>0) {
        return Messages.find({group: id});
    }
});
Meteor.publish('privateMessages', function (id) {
    if (!this.userId) {
        throw new Meteor.Error("Not authorized");
    }
    check(id, String);

    return Messages.find({$or: [{from: this.userId, to: id}, {to: this.userId, from: id}]});
});