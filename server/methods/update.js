/**
 * Created by saria on 12/8/16.
 */
Meteor.methods({
    addUserToGroup(id){
        if (!this.userId) {
            throw new Meteor.Error("Not authorized");
        }
        check(id, String);

        if (Groups.find({$and: [{_id: id}, {$or: [{members: this.userId}, {owner: this.userId}]}]}).count() == 1) {
            throw new Meteor.Error("User is already in that group");
        }
        Groups.update({_id: id}, {
            $push: {
                members: this.userId
            }
        })
    },
    removeUserFromGroup(id){
        if (!this.userId) {
            throw new Meteor.Error("Not authorized");
        }
        check(id, String);
        if (Groups.find({_id: id, owner: this.userId}).count() === 1) {
            Groups.update({_id: id}, {
                $set: {owner: "Null"}
            });
        }
        Groups.update({_id: id}, {
            $pull: {
                members: this.userId
            }
        });

        if ((Groups.findOne({_id: id}).members && Groups.findOne({_id: id}).members.length == 0) && Groups.findOne({_id: id}).owner === "Null") {
            Groups.remove(id);
        } else if (!Groups.findOne({_id: id}).members && Groups.findOne({_id: id}).owner === "Null") {
            Groups.remove(id);
        }
    }
});