/**
 * Created by saria on 12/8/16.
 */
Meteor.methods({
    'addGroup': function (group) {
        if (!this.userId) {
            throw new Meteor.Error("Not authorized");
        }

        Groups.insert(group);
    },
    'insertMessage': function (message) {
        if (!this.userId) {
            throw new Meteor.Error("Not authorized");
        }
        if (message.to&&(message.from === message.to)) {
            throw new Meteor.Error("Sender cannot be same as receiver");
        }else if(message.group){

          let group=Groups.find({$and:[{_id:message.group}, {$or:[{members:this.userId},{owner:this.userId}]}]});
            console.log('here'+group.fetch());

            if(group.count()==0){
                throw new Meteor.Error("You are not a member of this group");
            }
        }
        Messages.insert(message);
    }
});