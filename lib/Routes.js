/**
 * Created by saria on 12/8/16.
 */
FlowRouter.route('/', {
    name: "chat",
    action: function () {
        BlazeLayout.render('chat');
    }
});
FlowRouter.route('/message/:id', {
    name: 'privateMessage',
    action: function(){
        BlazeLayout.render('chat', {yield: 'id'});}
});
FlowRouter.route('/group/:id', {
    name: 'groupMessage',
    action: function(){
        BlazeLayout.render('chat', {yield: 'id'});}
});
