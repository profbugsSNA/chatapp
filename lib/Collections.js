/**
 * Created by saria on 12/8/16.
 */

 //collection for groups
Groups = new Mongo.Collection('groups');

let GroupSchema = new SimpleSchema({
    'name': {
        type: String,
        label: "Name of the group"
    },
    'createdAt': {
        type: Date,
        autoValue: function () {
            if (this.isInsert) {
                return new Date;
            }
        }

    },
    'owner':{
      type:String,
        label:"User who created the group",
        autoValue:function () {
            if(this.isInsert){
                return Meteor.userId();
            }
        }
    },
    'members':{
        type:[String],
        label:"Members of this group",
        optional:true
    }
});
Groups.attachSchema(GroupSchema);


//collection for messages
Messages = new Mongo.Collection('messages');

let MessageSchema = new SimpleSchema({
    'from': {
        type: String,
        label: "The owner of the message",
        autoValue: function () {
            if (this.isInsert) {
                return Meteor.userId();
            }
        }
    },
    'to': {
        type: String,
        label: "The user whom the message is sent",
        optional: true
    },
    'group': {
        type: String,
        label: "The group to which message is sent",
        optional: true

    },
    'message': {
        type: String,
        label: "Message text"
    },
    'createdAt': {
        type: Date,
        label: "date of the message",
        autoValue: function () {
            if (this.isInsert) {
                return new Date;
            }
        }

    },

    'read': {
        type: Boolean,
        label: 'Identifies if message is read',
        autoValue: function () {
            if (this.isInsert) {
                return false;
            }
        }

    }
});
Messages.attachSchema(MessageSchema);


//collection for files
Files = new Mongo.Collection('files');
let FileSchema = new SimpleSchema({
    'name': {
        type: String,
        label: "Name of the file"
    },
    'createdAt': {
        type: Date,
        autoValue: function () {
            if (this.isInsert) {
                return new Date;
            }
        }

    },
    'owner':{
      type:String,
        label:"User who owns the file",
        autoValue:function () {
            if(this.isInsert){
                return Meteor.userId();
            }
        }
    }
});
Files.attachSchema(FileSchema);