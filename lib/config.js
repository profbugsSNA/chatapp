AccountsTemplates.addField({
    _id: 'username',
    type: 'text',
    displayName: "User name",
    required: true,
    func: function (value) {
        if (!Meteor.users.findOne({username:value}))
                return false; // meaning no error!
            return true; // Validation error!

    },
    errStr: 'User name exists!',
});